// @flow
import type { ConservationCollection } from '../../../types/conservation';

export type Store = {
  conservation?: ?ConservationCollection
};
